
module.exports = function(RED) {
    function SecretPassword(config) {
        
        RED.nodes.createNode(this,config);
        
        var node = this;
        
        node.on('input', function(msg) {
            
            msg.username=config.username; 
            msg.password=config.password;

            node.send(msg);
            
        });
    }
    RED.nodes.registerType("secret-password", SecretPassword);
}

